/* const menu = document.querySelector('.menu-btn')

menu.addEventListener('click',function () {
      menu.classList.toggle('.menu-btn_active')
}) */

const span = document.querySelector("span");
const classes = span.classList;

span.addEventListener('click',function () {
      const result = classes.toggle("c");

      if (result) {
            span.textContent = `'c' added; classList is now "${classes}".`;
      } else {
            span.textContent = `'c' removed; classList is now "${classes}".`;
      }
})